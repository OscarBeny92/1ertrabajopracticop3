
package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;

import javax.swing.UIManager;

public class pantalla {

	private final JPanel panelJuego = new JPanel();
	private final JPanel panelInicio = new JPanel();
	private final JPanel panelFin = new JPanel();
	private final JPanel panelGanaste = new JPanel();

	protected static final boolean KeyEvent_up = false;
	private JFrame frame;
	Tablero tablero;
	boolean empezar = false;
	boolean terminar = false;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public pantalla() {
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */

	private void mostrarInicio() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					panelInicio.setVisible(true);
					panelJuego.setVisible(false);
					panelFin.setVisible(true);
					panelGanaste.setVisible(false);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	private void mostrarJuego() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					panelJuego.setVisible(true);
					panelInicio.setVisible(false);
					panelFin.setVisible(false);
					panelGanaste.setVisible(false);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	private void mostrarFin() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					panelInicio.setVisible(false);
					panelJuego.setVisible(false);
					panelGanaste.setVisible(false);


					panelFin.setVisible(true);
					terminar = true;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}	
		});
	}
	private void MostrarGanaste() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					panelInicio.setVisible(false);
					panelJuego.setVisible(false);
					panelGanaste.setVisible(true);
					panelFin.setVisible(false);
					terminar = true;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}	
		});
	}


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					pantalla window = new pantalla();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	private void initialize() {
		tablero = new Tablero();
		frame = new JFrame();
		frame.setTitle("2048");
		frame.setFocusable(true);
		inicializarPanelJuego();
		iniciarPanelInicio();
		iniciarPanelFinal();
		iniciarPanelGanaste();

		mostrarInicio();

		frame.addKeyListener(new KeyAdapter() {
			@Override


			public void keyPressed(KeyEvent e) {
				if (( e.getKeyChar() == 'w' || e.getKeyCode() == KeyEvent.VK_UP) && empezar==true && terminar==false)
				{
					tablero.moverVertical("arriba");
					frame.requestFocus();       
					actualizarPanel();	
					juegoTerminado();
					actualizarPuntos();
				}
				else if ( (e.getKeyChar() == 's' || e.getKeyCode() == KeyEvent.VK_DOWN) && empezar==true && terminar==false)
				{
					tablero.moverVertical("abajo");
					frame.requestFocus();
					actualizarPanel();
					juegoTerminado();
					actualizarPuntos();
				}
				else if ( (e.getKeyChar() == 'a' || e.getKeyCode() == KeyEvent.VK_LEFT) && empezar==true && terminar==false)
				{
					tablero.moverHoriz("izquierda");
					frame.requestFocus();
					actualizarPanel();
					juegoTerminado();
					actualizarPuntos();
				}
				else if ( (e.getKeyChar() == 'd' || e.getKeyCode() == KeyEvent.VK_RIGHT) && empezar==true && terminar==false)
				{
					tablero.moverHoriz("derecha");
					frame.requestFocus();
					actualizarPanel();
					juegoTerminado();
					actualizarPuntos();
				}

			}

			@Override
			public void keyReleased(KeyEvent e) {
			}
			@Override
			public void keyTyped(KeyEvent e) {

			}
		});


		frame.requestFocus();
		frame.setBounds(100, 100, 556, 502);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

	}

	private void inicializarPanelJuego() {
		panelJuego.setOpaque(false);

		JPanel paneldeljuego = new JPanel();
		paneldeljuego.setBorder(null);
		paneldeljuego.setBounds(33, 35, 483, 400);
		paneldeljuego.setLayout(null);
		panelJuego.add(paneldeljuego);

		JButton btnNewButton1 = new JButton("");
		btnNewButton1.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton1.setBounds(10, 11, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton1, 0, 0);
		paneldeljuego.add(btnNewButton1);

		JButton btnNewButton2 = new JButton("");
		btnNewButton2.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton2.setBounds(126, 11, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton2, 1, 0);
		paneldeljuego.add(btnNewButton2);

		JButton btnNewButton3 = new JButton("");
		btnNewButton3.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton3.setBounds(242, 11, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton3, 2, 0);
		paneldeljuego.add(btnNewButton3);

		JButton btnNewButton4 = new JButton("");
		btnNewButton4.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton4.setBounds(358, 11, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton4, 3, 0);
		paneldeljuego.add(btnNewButton4);

		JButton btnNewButton5 = new JButton("");
		btnNewButton5.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton5.setBounds(10, 100, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton5, 0, 1);
		paneldeljuego.add(btnNewButton5);

		JButton btnNewButton6 = new JButton("");
		btnNewButton6.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton6.setBounds(126, 100, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton6, 1, 1);
		paneldeljuego.add(btnNewButton6);

		JButton btnNewButton7 = new JButton("");
		btnNewButton7.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton7.setBounds(242, 100, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton7, 2, 1);
		paneldeljuego.add(btnNewButton7);

		JButton btnNewButton8 = new JButton("");
		btnNewButton8.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton8.setBounds(358, 100, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton8, 3, 1);
		paneldeljuego.add(btnNewButton8);

		JButton btnNewButton9 = new JButton("");
		btnNewButton9.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton9.setBounds(10, 189, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton9, 0, 2);
		paneldeljuego.add(btnNewButton9);

		JButton btnNewButton10 = new JButton("");
		btnNewButton10.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton10.setBounds(126, 189, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton10, 1, 2);
		paneldeljuego.add(btnNewButton10);

		JButton btnNewButton11 = new JButton("");
		btnNewButton11.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton11.setBounds(242, 189, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton11, 2, 2);
		paneldeljuego.add(btnNewButton11);

		JButton btnNewButton12 = new JButton("");
		btnNewButton12.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton12.setBounds(358, 189, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton12, 3, 2);
		paneldeljuego.add(btnNewButton12);


		JButton btnNewButton13 = new JButton("");
		btnNewButton13.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton13.setBounds(10, 278, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton13, 0, 3);
		paneldeljuego.add(btnNewButton13);



		JButton btnNewButton14 = new JButton("");
		btnNewButton14.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton14.setBounds(126, 278, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton14, 1, 3);
		paneldeljuego.add(btnNewButton14);


		JButton btnNewButton15 = new JButton("");
		btnNewButton15.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton15.setBounds(242, 278, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton15, 2, 3);
		paneldeljuego.add(btnNewButton15);

		JButton btnNewButton16 = new JButton("");
		btnNewButton16.setFont(new Font("Tahoma", Font.PLAIN, 38));
		btnNewButton16.setBounds(358, 278, 106, 78);
		pantalla.this.marcarCasillero(btnNewButton16, 3, 3);
		paneldeljuego.add(btnNewButton16);

		JLabel lblpuntos = new JLabel("Puntos: " + tablero.getPuntos());
		lblpuntos.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblpuntos.setBounds(186, 367, 117, 22);

		paneldeljuego.add(lblpuntos);
		panelJuego.setBounds(0, 0, 694, 471);
		frame.getContentPane().add(panelJuego);
		panelJuego.setLayout(null);

	}

	private void iniciarPanelInicio() {
		panelInicio.setOpaque(false);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(pantalla.class.getResource("/interfaz/fondo-de-numeros.png")));
		lblNewLabel.setBounds(0, 0, 540, 463);
		panelInicio.add(lblNewLabel);

		JPanel paneldenicio = new JPanel();
		paneldenicio.setBounds(10, 20, 520, 414);
		paneldenicio.setLayout(null);
		panelInicio.add(paneldenicio);

		JButton btnComenzar = new JButton("Comenzar Juego");
		btnComenzar.setForeground(UIManager.getColor("Button.foreground"));
		btnComenzar.setFont(new Font("Times New Roman", Font.ITALIC, 22));
		btnComenzar.setBackground(UIManager.getColor("Button.darkShadow"));
		btnComenzar.setBounds(160, 155, 200, 80);
		btnComenzar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrarJuego();
				actualizarPanel();	
				empezar=true;
				btnComenzar.setVisible(false);

			}

		});

		panelInicio.add(btnComenzar);


		panelInicio.setBounds(0, 0, 694, 471);
		frame.getContentPane().add(panelInicio);
		panelInicio.setLayout(null);

	}

	private void iniciarPanelFinal() {


		panelFin.setOpaque(false);
		panelFin.setBorder(null);
		panelFin.setBounds(75, 160, 0, 150);

		JLabel pfjTitulo = new JLabel();
		pfjTitulo.setText("NO HAY MAS MOVIMIENTOS" );

		pfjTitulo.setFont(new Font("Segoe Print", Font.PLAIN, 30));
		pfjTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		pfjTitulo.setBounds(47, 11, 466, 111);
		panelFin.add(pfjTitulo);

		panelFin.setBounds(0, 0, 694, 471);
		frame.getContentPane().add(panelFin);
		panelFin.setLayout(null);

		JButton btnJugarDeNuevo = new JButton("Reiniciar");
		btnJugarDeNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				main(null);

			}
		});
		btnJugarDeNuevo.setFocusable(false);
		btnJugarDeNuevo.setBorderPainted(false);
		btnJugarDeNuevo.setOpaque(false);
		btnJugarDeNuevo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnJugarDeNuevo.setBounds(195, 200, 150, 50);
		panelFin.add(btnJugarDeNuevo);
	}

	private void iniciarPanelGanaste() {

		panelGanaste.setOpaque(false);
		panelGanaste.setBorder(null);
		panelGanaste.setBounds(75, 160, 0, 150);


		JLabel pfjTitulo = new JLabel();
		pfjTitulo.setText("Ganaste" );

		pfjTitulo.setFont(new Font("Segoe Print", Font.PLAIN, 30));
		pfjTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		pfjTitulo.setBounds(47, 11, 466, 111);
		panelGanaste.add(pfjTitulo);

		panelGanaste.setBounds(0, 0, 694, 471);
		frame.getContentPane().add(panelGanaste);
		panelGanaste.setLayout(null);



		JLabel puntaje = new JLabel();
		puntaje.setVerticalAlignment(SwingConstants.BOTTOM);
		puntaje.setHorizontalAlignment(SwingConstants.RIGHT);
		puntaje.setFont(new Font("Tahoma", Font.BOLD, 16));
		puntaje.setBounds(47, 244, 462, 135);
		puntaje.setText("");

		JButton btnJugarDeNuevo = new JButton("Reiniciar");
		btnJugarDeNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				main(null);

			}
		});
		btnJugarDeNuevo.setFocusable(false);
		btnJugarDeNuevo.setBorderPainted(false);
		btnJugarDeNuevo.setOpaque(false);
		btnJugarDeNuevo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		btnJugarDeNuevo.setBounds(195, 200, 150, 50);
		panelGanaste.add(btnJugarDeNuevo);
		panelGanaste.add(puntaje);
	}

	private void actualizarPanel() {

		tablero.generarAleatorio();
		JPanel panelTablero = (JPanel)panelJuego.getComponent(0);		
		Component[] casilleros = panelTablero.getComponents();
		int fila = 0;
		int columna = 0;
		for(int i = 0; i < casilleros.length; i++) {

			if(casilleros[i] instanceof JButton) {
				JButton casillero = (JButton) casilleros[i];
				int valor = tablero.retornaPosicion(fila, columna);
				String valorString= String.valueOf(valor);
				repintarCasillas(valor, casillero);
				casillero.setText(valorString);
				if(valor==0) {
					casillero.setText("");
				}
				if(fila<3)
					fila++;
				else {
					columna++;
					fila=0;
				}

			}		
		}

	}
	private String actualizarPuntos() {

		JPanel panelTablero = (JPanel)panelJuego.getComponent(0);		
		Component[] casilleros = panelTablero.getComponents();
		String puntos = "";

		for(int i = 0; i < casilleros.length; i++) {

			if(casilleros[i] instanceof JLabel) {

				JLabel casillero = (JLabel) casilleros[i];
				int valor = tablero.getPuntos();
				String valorString= String.valueOf(valor);

				casillero.setText("Puntos: " + valorString);
				puntos = casillero.getText();
			}		
		}
		return puntos;

	}   



	private void repintarCasillas(int valor, JButton casillero) {
		if(valor==2) {
			casillero.setBackground(new Color(255, 160, 122));
			casillero.setOpaque(true);
			casillero.setBorderPainted(false);
			casillero.setForeground(Color.WHITE);						
		}

		if(valor==4) {
			casillero.setBackground(new Color(72, 209, 204));
			casillero.setOpaque(true);
			casillero.setBorderPainted(false);
			casillero.setForeground(Color.WHITE);						
		}

		if(valor==8) {
			casillero.setBackground(new Color(219, 112, 147));
			casillero.setOpaque(true);
			casillero.setBorderPainted(false);
			casillero.setForeground(Color.WHITE);						
		}

		if(valor==16) {
			casillero.setBackground(new Color(250, 250, 210));
			casillero.setOpaque(true);
			casillero.setBorderPainted(false);
			casillero.setForeground(Color.WHITE);						
		}
		if(valor==32) {
			casillero.setBackground(new Color(255, 192, 203));
			casillero.setOpaque(true);
			casillero.setBorderPainted(false);
			casillero.setForeground(Color.WHITE);						
		}

		if(valor==64) {
			casillero.setBackground(new Color(135, 206, 250));
			casillero.setOpaque(true);
			casillero.setBorderPainted(false);
			casillero.setForeground(Color.WHITE);						
		}

		if(valor==128) {
			casillero.setBackground(new Color(221, 160, 221));
			casillero.setOpaque(true);
			casillero.setBorderPainted(false);
			casillero.setForeground(Color.WHITE);						
		}

		if(valor==256) {
			casillero.setBackground(new Color(173, 255, 47));
			casillero.setOpaque(true);
			casillero.setBorderPainted(false);
			casillero.setForeground(Color.WHITE);						
		}

		if(valor==512) {
			casillero.setBackground(new Color(153, 102, 255));
			casillero.setOpaque(true);
			casillero.setBorderPainted(false);
			casillero.setForeground(Color.WHITE);						
		}

		if(valor==1024) {
			casillero.setBackground(new Color(204, 102, 51));
			casillero.setOpaque(true);
			casillero.setBorderPainted(false);
			casillero.setForeground(Color.WHITE);						
		}
		if(valor==2048) {
			casillero.setBackground(new Color(0, 0, 0));
			casillero.setOpaque(true);
			casillero.setBorderPainted(false);
			casillero.setForeground(Color.WHITE);						
		}

		if(valor==0) {
			casillero.setText("");
			casillero.setBackground(null);
			casillero.setBorderPainted(false);
		}
	}

	private void juegoTerminado() {
		if (tablero.verificarResultado()) {
			JLabel puntaje = new JLabel();
			puntaje.setFont(new Font("Tahoma", Font.BOLD, 16));
			puntaje.setBounds(47, 244, 462, 135);
			puntaje.setText( actualizarPuntos());
			panelGanaste.add(puntaje);
			MostrarGanaste();
		}
		if(tablero.tieneMovimientos()){                     
			JLabel puntaje = new JLabel();
			puntaje.setFont(new Font("Tahoma", Font.BOLD, 16));
			puntaje.setBounds(47, 244, 462, 135);
			puntaje.setText(actualizarPuntos());
			panelFin.add(puntaje);
			mostrarFin();
		}
	}

	private void marcarCasillero(JButton casillero, int pos1, int pos2) {

		int valor = tablero.retornaPosicion(pos1, pos2);
		if(valor!=0) {
			String valorString= String.valueOf(valor);
			casillero.setText(valorString);
		}	
	}
}
