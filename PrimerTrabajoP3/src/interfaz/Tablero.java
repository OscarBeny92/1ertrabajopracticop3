package interfaz;

public class Tablero {

	private int[][] tablero = new int[4][4];
	public int puntos = 0;


	public int getPuntos() {
		return puntos;
	}

	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}

	public Tablero() {
		int[] pos1 = posRandom();
		marcarCasillero(pos1[0], pos1[1]);
	}

	public int retornaPosicion (int column, int fila) {
		int numero = 0;
		for(int i = 0; i<tablero.length ; i++) {
			if(tablero[i]==tablero[fila]) {
				for(int j = 0; j<tablero[i].length ; j++) {
					if(tablero[j]==tablero[column]) {
						numero = tablero[fila][column];
					}
				}
			}
		}
		return numero;
	}

	// funcion GANADOR 
	public boolean ganador () {
		//{
		boolean perdio = false;
		for(int i = 0; i<tablero.length ; i++) {
			for(int j = 0; j<tablero[i].length ; j++) {
				perdio = perdio || tablero[i][j]==64;

			}
		}
		return perdio;		
	}

	public boolean tieneMovimientos() {
		boolean noHayCero = true;
		{
			for(int i = 0; i<tablero.length ; i++) {
				for(int j = 0; j<tablero[i].length ; j++) {
					noHayCero = noHayCero && tablero[i][j] != 0;
				}
			}
		}
		return noHayCero;		
	}		

	// retorna verdadero en caso de que en alguna posicion se logre el 2048
	public boolean verificarResultado() {
		for(int i = 0; i<tablero.length ; i++) {
			for(int j = 0; j<tablero[i].length ; j++) {
				if(tablero[i][j] == 2048) {
					return true;}
			}
		}
		return false;
	}


	// verificamos que la matriz tenga elemento cero es decir lugar disponible para ingresar dato
	public boolean casilleroDisponible(int pos1,int pos2) {
		if(tablero[pos1][pos2] == 0) {
			return true;
		}
		return false;
	}
	//metodo que llama a numero aleatorio // el else esta de mas lo puse para ver que no encontro lugar dispoble (despuesborrar)
	public void marcarCasillero(int pos1,int pos2) {
		if(casilleroDisponible(pos1, pos2)) {
			tablero[pos1][pos2] = NumAleatorio();
		}
	}

	//genera un numero random puede ser un 2 o un 4
	public int NumAleatorio () {
		int numero = (int)(Math. random()*4+1);
		if (numero == 3 || numero == 1 ){
			return NumAleatorio();
		}
		return numero;
	}
	// nos devuelve una posicion dentro de la grilla de forma aleatoria
	public int[] posRandom () {
		int[] tabl = null;
		tabl = new int [2];
		int num1 = (int)(Math. random()*4+0);
		int num2 = (int)(Math. random()*4+0);
		tabl[0] = num1;
		tabl[1] = num2;
		return tabl ;

	}
	//Genera una posicion aleatoria
	public void generarAleatorio() {
		int[] pos1;
		//Control para que no se quede ciclando

		if(!tieneMovimientos()) {
			do {
				pos1 = this.posRandom();
			} while (tablero[pos1[0]][pos1[1]]!=0);
			this.marcarCasillero(pos1[0], pos1[1]);
		}
	}

	//luego de un mov vertical verifica si dos celdas juntas son iguales y las suma
	public void sumarVertical() {
		for(int i = 0; i<tablero.length-1 ; i++) {
			for(int j = 0; j<tablero[i].length-1 ; j++) {
				if(tablero[i][j]==tablero[i+1][j]) {
					tablero[i][j]=tablero[i][j]*2;
					tablero[i+1][j]=0;
				}
			}
		}
	} 


	//Luego de un mov a la derecha verifica si dos celdas juntas son iguales y las suma

	public void moverHoriz(String direccion) {
		for(int i=0; i<tablero.length ; i++) {
			int anterior = 0;
			if(direccion.equals("derecha")) {
				for(int j=0; j<tablero[i].length-1 ; j++) {

					if((tablero[i][j]== tablero[i][j+1]) || tablero[i][j+1] ==0) {
						puntos += tablero[i][j+1];

						tablero[i][j+1] = tablero[i][j+1] + tablero[i][j];
						tablero[i][j] = anterior;
						if(j>0) {
							tablero[i][j-1] = 0;


						}
					}
					else {
						anterior = tablero[i][j];
					}
				}
			}
			else {
				for(int j=tablero[i].length-1 ; j>0; j--) {

					if((tablero[i][j]== tablero[i][j-1]) || tablero[i][j-1] ==0) {
						puntos +=tablero[i][j-1];
						puntos = puntos +15;
						tablero[i][j-1] = tablero[i][j-1] + tablero[i][j];
						tablero[i][j] = anterior;

						if(j<3) {
							tablero[i][j+1] = 0;
						}
					}
					else {
						anterior = tablero[i][j];
					}
				}
			}

		}
	}

	public void moverVertical(String direccion) {
		for(int j=0; j<tablero.length ; j++) {
			int anterior = 0;
			if(direccion.equals("abajo")) {
				for(int i=0; i<tablero[j].length-1 ; i++) {

					if((tablero[i][j]== tablero[i+1][j]) || tablero[i+1][j] ==0) {
						puntos += tablero[i+1][j];
						tablero[i+1][j] = tablero[i+1][j] + tablero[i][j];
						tablero[i][j] = anterior;

						if(i>0) {
							tablero[i-1][j] = 0;
						}
					}
					else {
						anterior = tablero[i][j];
					}
				}
			}
			else {
				for(int i=tablero[j].length-1 ; i>0; i--) {

					if((tablero[i][j]== tablero[i-1][j]) || tablero[i-1][j] ==0) {
						puntos += tablero[i-1][j];

						tablero[i-1][j] = tablero[i-1][j] + tablero[i][j];
						tablero[i][j] = anterior;
						if(i<3) {
							tablero[i+1][j] = 0;
						}
					}
					else {
						anterior = tablero[i][j];
					}
				}
			}

		}
	}

}






